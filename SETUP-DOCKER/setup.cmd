#!/bin/bash
cd SETUP-DOCKER
if [ -f .env ]; then
  export $(echo $(cat .env | sed 's/#.*//g'| xargs) | envsubst)
fi

docker-compose up -d
cd ..
echo "!! SETUP is DONE - SYSTEM is RUNNING !!"
