<?php
namespace CONFIG;

//--- DATABASE --------------------------------------------------------------------------
CONST GS_DB_ADDRESS     = "database";
CONST GS_DB_NAME        = "duematch";
CONST GS_DB_USERNAME    = "devuser";
CONST GS_DB_PASSWORD    = "password";
CONST GS_DB_PORT        = "3306";