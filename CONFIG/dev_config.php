<?php
namespace CONFIG;

CONST GS_API_DEBUG              = FALSE;
CONST GLOBAL_NO_DB              = FALSE;
CONST GS_SYSTEM_HAS_ORIGIN_DB   = FALSE;
CONST GS_CAN_LOG_FAULTY_QUERY   = FALSE;
CONST GS_CAN_DEBUG_QUERY        = TRUE;

//--- DATABASE --------------------------------------------------------------------------
CONST GS_DB_ADDRESS     = "192.168.1.175";
CONST GS_DB_NAME        = "duematch";
CONST GS_DB_USERNAME    = "worker";
CONST GS_DB_PASSWORD    = "password";
CONST GS_DB_PORT        = "3306";
