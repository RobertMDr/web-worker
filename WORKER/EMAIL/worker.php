<?php

CONST PAUSE_BETWEEN_JOBS                = 1;
CONST PAUSE_IF_NO_JOBS                  = 5;
CONST CALCULATE_INTERVAL_EVERY_MINUTES  = 30;

CONST WORKER_TYPE = "EMAIL";

echo "    ________  ______    ______
   / ____/  |/  /   |  /  _/ / 
  / __/ / /|_/ / /| |  / // /  
 / /___/ /  / / ___ |_/ // /___
/_____/_/  /_/_/  |_/___/_____/
                               
\n";

echo "STARTING:\n";
echo "CONNECTION TO JOB POOL: ";
require_once("CORE/connectdb.php");
echo "DONE\n";

$index = 0;

$intervalStatistics = time();

while(1){
    $intervalDif = CALCULATE_INTERVAL_EVERY_MINUTES*60-(time()-$intervalStatistics);
    if($intervalDif<=0){
        $resultProcessedJobs = qr("SELECT * FROM `jobs` WHERE `type_job`='".WORKER_TYPE."' AND `processed_job`=1 ORDER BY `processedStartTimestamp_job` ASC LIMIT 30");

        $index = 0;
        $diffTime = 0;

        while($row = mysqli_fetch_assoc($resultProcessedJobs)){
            $index++;
            $startProcessTime = strtotime($row["processedStartTimestamp_job"]);
            $endProcessTime = strtotime($row["processedEndTimestamp_job"]);
            $diffTime += $endProcessTime-$startProcessTime;
        }

        $temp = dqr("REPLACE INTO `jobStatistics` 
          (`jobType_jobStatistic`, `timeSpent_jobStatistic`) VALUES
          ('".WORKER_TYPE."', ".($diffTime/$index).")
          ");
        \COREWORKER\echoCLIWorker(mysqli_error($DBCONNECTION));
        \COREWORKER\echoCLIWorker("Average time spent by worker: ".($diffTime/$index));

        \COREWORKER\echoCLIWorker("Calculating");
        $intervalStatistics = time();
        continue;
    }

    \COREWORKER\echoCLIWorker("Calculating interval in ".($intervalDif/60)."m");


    $jobReserved = \COREWORKER\reserveJob(WORKER_TYPE);

    if($jobReserved == FALSE){
        \COREWORKER\echoCLIWorker("NO JOBS FOUND ! ");
        \COREWORKER\echoCLIWorker("Sleeping for ".PAUSE_IF_NO_JOBS."s");
        sleep(PAUSE_IF_NO_JOBS);
        continue;
    }

    \COREWORKER\echoCLIWorker("EMAIL WORKER: sleeping for ".PAUSE_BETWEEN_JOBS."s");
    sleep(PAUSE_BETWEEN_JOBS);

    \COREWORKER\echoCLIWorker("process");

    \WORKER_EMAIL\processEmail($jobReserved);
}