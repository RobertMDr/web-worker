<?php
$configLoaded = false;

if(file_exists("CONFIG/dev_config.php")) {
    require_once("CONFIG/dev_config.php");
    $configLoaded = true;
}

if(!$configLoaded && file_exists("CONFIG/prod_config.php")){
    require_once("CONFIG/prod_config.php");
    $configLoaded = true;
}

if(!$configLoaded){
    die("I got some milk, eggs and fabric softener, but no config !");
}

require_once("APP/CONFIG/app_config.php");

require_once("CORE/coreFunctions.php");
require_once("CORE/customFunctions.php");
require_once("CORE/coreActions.php");
require_once("CORE/coreErrorReporting.php");
require_once("CORE/coreWorkerFunction.php");
require_once("CORE/worker-email.php");

echo ("\n\n###########################");
echo (  "\n# DUEMATCH WORKER         #");
echo (  "\n###########################\n\n");

$shortOptions = "worker:";
$longOptions = [
    "worker:"
];

$options = getopt(NULL, $longOptions);

if(!$options["worker"]){
    die("No workers specified\n\n");
}

if(in_array($options["worker"], \APP_CONFIG\PLATFORM_JOB_HANDLERS) == FALSE ){
    die("Worker does not match Config\n\n");
}
require_once ("WORKER/".$options["worker"]."/worker.php");
echo "\n";
